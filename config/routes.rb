Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :restaurants
  devise_for :users
  get 'home/index'
  get 'home/partner_with_us'
  root 'home#index'
  resources :users
  resources :restaurants
  get 'restaurant/:id', to: 'users#show_restaurant', as: "show_restaurant"
  namespace :user do
    resources :cart
  end
  resources :connect_requests
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
