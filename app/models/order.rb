class Order < ApplicationRecord
  has_many :comments
  belongs_to :food
  belongs_to :placed_order
end
