class ConnectRequest < ApplicationRecord
  after_update :create_restaurant, if: :saved_change_to_approved?

  def create_restaurant
    puts "Restaurant created"
    @restaurant = AdminUser.new
    @restaurant.name = self.restaurant_name
    @restaurant.email = self.email
    @restaurant.mobile_number = self.mobile_number
    @restaurant.password = 123456 
    @restaurant.password_confirmation = 123456
    @restaurant.role = 1
    byebug
    if @restaurant.save
      AdminMailer.with(restaurant: @restaurant).restaurant_created.deliver_now
    end
  end

end
