class Address < ApplicationRecord
  has_many :address_assigns

  has_many :users, :through => :address_assigns, :source => :addressable,
           :source_type => 'User'
  has_one :admin_users, :through => :address_assign, :source => :addressable,
           :source_type => 'AdminUser'

  has_many :placed_orders
  enum address_type: [:home, :office, :other]
end
