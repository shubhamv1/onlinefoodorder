class PlacedOrder < ApplicationRecord
  has_many :orders
  belongs_to :restaurant, class_name: "AdminUser", foreign_key: "restaurant_id"
  belongs_to :user
  enum status: [:placed, :in_review, :confirm, :prepare, :out_for_delivery, :delivered]
  enum payment_mode: [:cash, :paytm, :google_pay, :phone_pe, :amazon_pay, :gift_card, :card]
  enum payment_status: [:incomplete, :done]
  has_many :comments


private

end
