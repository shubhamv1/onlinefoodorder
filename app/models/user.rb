class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  after_update :create_cart, if: :saved_change_to_confirmed_at?
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable
  has_many :address_assigns, :as => :addressable
  has_many :addresses, :through => :address_assigns
  
   has_many :placed_orders
   has_many :comments
   has_one :cart
   
   def to_s
     "#{name}"
   end

   def create_cart
    cart = Cart.new
    self.cart = cart
    puts "cart created"
   end

   

end
