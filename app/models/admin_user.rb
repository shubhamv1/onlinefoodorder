class AdminUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :validatable
  enum role: [:admin, :restaurant]

  has_one :address_assign, :as => :addressable
  has_one :address, :through => :address_assign
  has_many :foods, foreign_key: "restaurant_id", class_name:"Food"
  has_many :rating_and_reviews, foreign_key: "restaurant_id", class_name:"RatingAndReview"
  has_many :placed_orders, foreign_key: "restaurant_id", class_name:"PlacedOrder"
  
end
