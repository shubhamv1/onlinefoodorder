class RatingAndReview < ApplicationRecord
  belongs_to :restaurant, class_name: "AdminUser", foreign_key: "restaurant_id"
end
