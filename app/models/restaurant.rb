class Restaurant < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_secure_token :confirmation_token
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable
  has_one :address_assign, :as => :addressable
  has_one :address, :through => :address_assign
  has_many :foods
  has_many :rating_and_reviews 
  has_many :placed_orders
  def to_s
  "#{name}"
  end
end
