class UsersController < ApplicationController
  def index
    @foods = Food.all
  end

  def show_restaurant
    @restaurant = Restaurant.find_by_id(params[:id]);
    @foods = @restaurant.foods
    @grouped_foods = @foods.group_by {|food| food.food_catlog}
    @grouped_foods.each do |catlog, array|
      puts array
    end
  end
end
