class ConnectRequestsController < ApplicationController
  def new
    @connect_request = ConnectRequest.new
  end

  def create
    byebug
    @connect_request = ConnectRequest.new(connect_request_params)
    if @connect_request.save
      redirect_to home_partner_with_us_path
    else
      render :new
    end
  end

  def update
    @connect_request = ConnectRequest.find_by_id(params[:id])
    @connect_request.approved = true
    @connect_request.save
    byebug
  end

  private
  def connect_request_params
    params.require(:connect_request).permit(:restaurant_name, :owner_name, :mobile_number, :email, :current_city, :rating)
  end

end
