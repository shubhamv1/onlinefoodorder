ActiveAdmin.register Address do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :country, :state, :city, :postal_code, :house_number, :building_name, :area, :landmark, :address_type, :default
  #
  # or
  #
  permit_params do
    permitted = [:country, :state, :city, :postal_code, :house_number, :building_name, :area, :landmark, :address_type, :default]
    permitted
  end

  form do |f|
    f.inputs do
      f.input :house_number
      f.input :building_name
      f.input :area
      f.input :postal_code
      f.input :landmark
      f.input :default  
    end
    f.actions
  end
  
  controller do
    def create
      create! do |format|
        format.html { redirect_to "/admin/#{current_admin_user.role}s/#{current_admin_user.id}"}
      end
    end
  end
end
