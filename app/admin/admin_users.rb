ActiveAdmin.register AdminUser, as: "Restaurant" do
  menu label: 'Restaurant', priority: 3
  scope :restaurant, default: true do |user|
    user = AdminUser.restaurant
  end

  controller do
    def scoped_collection
      AdminUser.restaurant
    end
  end

  permit_params :email, :password, :password_confirmation, :name, :mobile_number, :role

  index do
    selectable_column
    id_column
    column :name
    column :mobile_number
    column :email
    actions
  end

  filter :role
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :name, label: "Restaurant Name"
      f.input :email
      f.input :mobile_number
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

action_item :add do
  link_to "Add Address", new_admin_address_path, method: :get
end

end

ActiveAdmin.register AdminUser, as: "Admin" do
  menu label: 'Admin', priority: 3
  scope :admin, default: true do |user|
    user = AdminUser.admin
  end

  controller do
    def scoped_collection
      AdminUser.admin
    end
  end

  
  permit_params :email, :password, :password_confirmation, :name, :mobile_number, :role

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :role
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    byebug
    f.inputs do
      f.input :name
      f.input :email
      f.input :mobile_number
      f.input :role
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end


