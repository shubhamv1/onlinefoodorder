ActiveAdmin.register User do
  after_create { |user| user.send_reset_password_instructions }
    def password_required?
      new_record? ? false : super
    end
sidebar 'Your Addresses', :only => :show do
  table_for user.addresses do |t|
    t.column("house_number") { |address| address.house_number} 
    t.column("building_name") { |address| address.building_name } 
  end
end
  sidebar 'User Orders', :only => :show do
  table_for user.placed_orders do |t|
    t.column("restaurant") { |po| po.restaurant}  
    t.column("amount") { |po| po.net_amount}  
  end
end    
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# Uncomment all parameters which should be permitted for assignment
#
permit_params :email, :name, :mobile_number, :addresses_id, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :confirmation_token, :confirmed_at, :confirmation_sent_at
#
# or
#
# permit_params do
#   permitted = [:email, :name, :mobile_number, :addresses_id, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :confirmation_token, :confirmed_at, :confirmation_sent_at]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
form do |f|
f.inputs "Admin Details" do
  f.input :email
end
f.actions
end
index do
column :name
column :email
column :mobile_number
actions
end
end
