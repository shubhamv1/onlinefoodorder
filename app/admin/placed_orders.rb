ActiveAdmin.register PlacedOrder do
  sidebar 'Foods of this Order', :only => :show do
    table_for Order.joins(:placed_order).where(:placed_order_id => placed_order.id) do |t|
      t.column("food") { |order| order.food }
      t.column("qty") { |order| order.qty }
      t.column("price") { |order| order.price }
      t.column("amount") { |order| order.amount }
    end
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :restaurant_id, :user_id, :address_id, :gross_amount, :discount, :net_amount, :payment_mode, :payment_status, :status
  #
  # or
  #
  # permit_params do
  #   permitted = [:restaurant_id, :user_id, :address_id, :gross_amount, :discount, :net_amount, :payment_mode, :payment_status, :status]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  index do
    column :id
    column :restaurant
    column :user
    column :gross_amount
    column :discount
    column :net_amount
    column :payment_mode
    column :payment_status
    column :status
    actions
  end
end
