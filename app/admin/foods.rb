ActiveAdmin.register Food do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :name, :description, :restaurant_id, :price, :food_catlog, :status, :veg_or_non_veg
  #
  # or
  #
  permit_params do
    permitted = [:name, :description, :restaurant_id, :price, :food_catlog, :status, :veg_or_non_veg]
    permitted << :other if params[:action] == 'create' && current_admin_user.restaurant?
    permitted
  end

  form do |f|
    f.object.restaurant_id = current_admin_user.id
    f.inputs do
      f.input :name
      f.input :description
      f.input :price
      f.input :food_catlog
      f.input :status
      f.input :veg_or_non_veg
      f.input :restaurant_id, as: :hidden
    end
    f.actions
  end
  index do
    column :name
    column :description
    column :price
    column :food_catlog
    column :veg_or_non_veg
    column :restaurant
    actions
  end
end
