ActiveAdmin.register AddressAssign do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :addressable_id, :addressable_type, :address_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:addressable_id, :addressable_type, :address_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
