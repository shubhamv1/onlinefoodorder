ActiveAdmin.register ConnectRequest do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :restaurant_name, :owner_name, :mobile_number, :email, :current_city, :rating, :approved
  #
  # or
  #
  # permit_params do
  #   permitted = [:restaurant_name, :owner_name, :mobile_number, :email, :current_city, :rating]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  show :title => :restaurant_name do     
    attributes_table do
      row :restaurant_name
      row :owner_name
      row :email
      row :mobile_number
      row :current_city
      row :rating
      if connect_request.approved != true
        row :approve do
          link_to('click for approve',connect_request_path(connect_request.id), method: :put, :class => "status_tag yes")
        end
      else
        row :approved
      end
    end
  end
end
