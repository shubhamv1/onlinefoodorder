class CreateFoods < ActiveRecord::Migration[6.1]
  def change
    create_table :foods do |t|
      t.string :name
      t.text :description 
      t.bigint :restaurant_id
      t.integer :price
      t.integer :food_catlog
      t.integer :status, default: 0
      t.string :veg_or_non_veg
      t.timestamps
    end
  end
end
