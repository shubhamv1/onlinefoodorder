class CreateAddressAssigns < ActiveRecord::Migration[6.1]
  def change
    create_table :address_assigns do |t|
      t.bigint :addressable_id 
      t.string :addressable_type 
      t.bigint :address_id
      t.timestamps
    end
  end
end
