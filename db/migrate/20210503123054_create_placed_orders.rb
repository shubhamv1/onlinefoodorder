class CreatePlacedOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :placed_orders do |t|
        t.bigint :restaurant_id
        t.bigint :user_id
        t.bigint :address_id
        t.integer :gross_amount
        t.integer :discount
        t.integer :net_amount
        t.integer :payment_mode
        t.integer :payment_status, default: 0
        t.integer :status, default: 0
      t.timestamps
    end
  end
end
