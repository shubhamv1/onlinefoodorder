class CreateAddresses < ActiveRecord::Migration[6.1]
  def change
    create_table :addresses do |t|
      t.string :country, default: "India"
      t.string :state, default: "Madhya Pradesh"
      t.string :city, default: "Indore"
      t.integer :postal_code
      t.integer :house_number
      t.string :building_name
      t.string :area
      t.string :landmark
      t.integer :address_type, default:0
      t.boolean :default, default:false
      t.timestamps
    end
  end
end