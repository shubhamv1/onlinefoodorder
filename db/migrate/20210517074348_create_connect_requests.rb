class CreateConnectRequests < ActiveRecord::Migration[6.1]
  def change
    create_table :connect_requests do |t|
        t.string :restaurant_name, null: false
        t.string :owner_name, null: false
        t.string :mobile_number, null: false, index: { unique: true }
        t.string :email, null: false, index: { unique: true }
        t.string :current_city, null: false
        t.integer :rating, null: false
        t.boolean :approved, default:false 
      t.timestamps
    end
  end
end
