class CreateCartItems < ActiveRecord::Migration[6.1]
  def change
    create_table :cart_items do |t|
      t.belongs_to :cart
      t.bigint :food_id
      t.integer :qty
      t.timestamps
    end
  end
end
